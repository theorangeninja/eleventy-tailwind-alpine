const now = String(Date.now())

module.exports = function (eleventyConfig) {
  eleventyConfig.setUseGitIgnore(false)

  eleventyConfig.addWatchTarget('./src/_tmp/style.css')

  eleventyConfig.addPassthroughCopy({ './src/_tmp/style.css': './assets/css/style.css' })

  eleventyConfig.addPassthroughCopy({
    './node_modules/alpinejs/dist/cdn.js': './assets/js/alpine.js',
  })

  eleventyConfig.addShortcode('version', function () {
    return now
  })

  return {
    dir: {
      input: "src",
      output: "public",
    },
  };
};