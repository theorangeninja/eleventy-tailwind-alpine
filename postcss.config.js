module.exports = {
  plugins: {
    tailwindcss: './styles/tailwind.config.js',
    autoprefixer: {},
    ...(process.env.NODE_ENV === 'production' ? { cssnano: {} } : {}),
  },
}
